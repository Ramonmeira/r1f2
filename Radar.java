package rlm;

import java.util.*;
import rlm.Util;
import rlm.GravPoint;

/**
 * All methods related to the Radar
 * <p>
 */
final class Radar {
	private R1F2 bot;
	private int radarDirection = 1;

	public Radar(R1F2 _bot){
		bot = _bot;
	}

	public void sweep(){
		double maxBearingAbs = 0, maxBearing = 0;
		int scannedBots = 0;
		GravPoint enemy;
		Enumeration e = bot.enemyManager.getTargets().elements();
		while (e.hasMoreElements()) {
    	    enemy = (GravPoint)e.nextElement();
			if (enemy!=null && enemy.isUpdated(bot.getTime()) && (enemy instanceof GravPointEnemy)) {
				double bearing = Util.normaliseBearing(bot.getHeadingRadians() + enemy.bearing - bot.getRadarHeadingRadians());
				if (Math.abs(bearing) > maxBearingAbs) {
					maxBearingAbs = Math.abs(bearing);
					maxBearing = bearing;
				}
				scannedBots++;
			}
		}

		double radarTurn = Math.PI * radarDirection;
		if (scannedBots == bot.getOthers()){
			radarTurn = maxBearing + Util.sign(maxBearing) * Math.PI/4;
		}
		radarDirection = Util.sign(radarTurn);
		bot.setTurnRadarRightRadians(radarTurn);	
	}

}


/*
Gathering information
The following are levels of scanning:

Simple. Just point the radar in the same direction as the gun.
Circular. Keep the radar continually turning.
Tracking. Keep the radar locked onto a single target.
Optimal. Move the radar in such a way as to scan all of the robots as quickly as possible.
In more advanced robots, the data that can be collected about the other robots on the battlefield is vital in making sophisticated strategies for both movement and firing. The following lists some of the data that can be obtained about other robots:

Position
Heading
Velocity
Angular velocity
Energy
How much damage you have done to them -- useful for comparing the other robots on the battlefield and targeting the one that you seem to be able to hit most (or least) easily
How much damage they did to you
How many times and when they have died
When they fire a bullet (tricky technique using energy differential)
This list of strategies is by no means complete. The top robots in the Gladiatorial League competition use most of the above strategies to some extent. However, every now and again someone comes up with a new concept or simple strategy that forces all of the other robots to come up with counter strategies. Even the simplest ideas can be very successful.
*/