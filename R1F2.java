package rlm;package rlm;


import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import robocode.*;

import java.awt.*;


/**
 * R1F2_2_0
 * Based on SpinBot has two changes:
 *	- Hit less the walls
 *  - Spinning in differents radius
 * <p>
 * Moves in a circle, firing hard when an enemy is detected.
 */
public class R1F2_2_0 extends AdvancedRobot {
	int maxVelocity = 5;
	int velocityAdjustment = 1;
	int direction = 1;

	/**
	 * SpinBot's run method - Circle
	 */
	public void run() {
		// Set colors
		setBodyColor(Color.blue);
		setGunColor(Color.blue);
		setRadarColor(Color.black);
		setScanColor(Color.yellow);
		
		// Limit our speed to 5
		setMaxVelocity(5);

		// Loop forever
		while (true) {
			// Tell the game that when we take move,
			// we'll also want to turn right... a lot.
			setTurnRight(10000*direction);			
			// Start moving (and turning)
			ahead(10000);
			// Repeat.
		}
	}

	/**
	 * onScannedRobot: Fire hard!
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(3);
		if(maxVelocity==8){
			velocityAdjustment = -1;
		}
		if(maxVelocity==5){
			velocityAdjustment = 1;
		}
		maxVelocity += velocityAdjustment;
		setMaxVelocity(maxVelocity);
	}

	/**
	 * onHitRobot:  If it's our fault, we'll stop turning and moving,
	 * so we need to turn again to keep spinning.
	 */
	public void onHitRobot(HitRobotEvent e) {
		if (e.getBearing() > -10 && e.getBearing() < 10) {
			fire(3);
		}
		if (e.isMyFault()) {
			turnRight(10);
		}
	}
	
	/**
	 * onHitWall:  Handle collision with wall.
	 */
	public void onHitWall(HitWallEvent e) {		
		setTurnRight(e.getBearing()-22.5*direction);
		direction = -direction;
		ahead(250*direction);	
	}
}
