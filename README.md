# Descrição
A ordem principal para o robo e ir em frente e girar o máximo que puder, com isso ele passa a girar num círculo. Quando ele detecta outro robo ele muda a velocidade máxima que ele consegue avançar, o que também muda o raio do giro e consequentemente desvia de um disparo calculado para a rota anterior. 
Quando colide com outro robo e a direção é de poucos graus tem liberdade para disparar, poís tem alta chance de acertar.
Quando atinge uma parede se afasta antes de começar a girar novamente para evitar outras colisões consecutivas.

## Pontos fortes
- Boa evasão
- Dispara menos vezes, o que poupa energia que seria gasta com disparos ruins.
- Não é tão aleatório que possa acertar as paredes de forma consecutiva.

## Pontos fracos
- Péssima mira.
- Péssimo uso do radar.
- Corpo, arma e radar giram em conjunto. 
- Não coleta informações sobre os oponentes.
- Dispara menos vezes, e consequentemente marca menos pontos por dano.

# Aprendizado
É fascinante como uma aplicação tão simples pode envolver tantos graus de complexidade.
Iniciantes podem começar com aspectos muito simples e fundamentos de programação enquando programadores mais avançados podem aplicar técnicas diferente para alcançar graus de complexidade cada vez maiores.

Li muitos textos e percebi os principais pontos de melhorias que podem haver nos robos: movimentação, mira, otimização do radar e análise dos inimigos. Por isso uma das partes que mais gostei foi implementar um robo modular, com classes para cada um desses elementos. Uma pena que o melhor dos robos que eu criei foi um dos mais simples, o fujão que ganha por resistência.

Revi muitos conceitos como: OO, padrões de projeto e a própria liguagem java.


Foi uma jornada muito prazerosa. Conseguir revolver os pequenos problemas que iam surgindo e tentar pensar formas de melhorar e aplicar as coisas que pesquisava foi divertido.

 #Quero ser DEV! Talent Sprint Solutis