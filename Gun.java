package rlm;

import java.awt.geom.*;

import rlm.Util;
import rlm.GravPoint;

/**
 * All methods related to the Gun
 * <p>
 */
public class Gun {
	private R1F2 bot;
	private double firePower;

	public Gun(R1F2 _bot){
		bot = _bot;
	}

	public void doGun(){
		doFirePower();
		doGunOffset();		
		bot.fire(firePower);
	}

	//selects a bullet power based on our distance away from the target
	private void doFirePower() {
		firePower = 400/bot.enemyManager.getTarget().distance;
		if (firePower > 3) {
			firePower = 3;
		}
	}

	private void doGunOffset() {
		GravPoint target = bot.enemyManager.getTarget();
		long timeImpact = bot.getTime() + (int)Math.round((Util.getRange(bot.getX(),bot.getY(),target.x,target.y)/Gun.getBulletVelocity(firePower)));
		Point2D.Double p = target.guessPosition(bot, timeImpact);
		
		//offsets the gun by the angle to the next shot based on linear targeting provided by the enemy class
		double gunOffset = bot.getGunHeadingRadians() - (Math.PI/2 - Math.atan2(p.y - bot.getY(), p.x - bot.getX()));
		bot.setTurnGunLeftRadians(Util.normaliseBearing(gunOffset));
	}

	public static double[] e2(double angle, double firePower){
		double velocidadeX = 0, velocidadeY = 0;

		double[] vector = {velocidadeX, velocidadeY};
		return vector;
	}

	public static double getBulletVelocity(double firePower){
		return (20-(3*firePower));		
	}

}

/*
Shooting
The following are levels of targeting:

Stationary. Shoot at the current location of the target robot. This is the simplest and least effective targeting strategy, as any movement of the robot causes the bullets to miss.
Linear. Shoot at where the target will be, assuming that it moves at a constant velocity in a straight line. This targeting method is very effective, especially when the target is relatively close. All robot motion can be approximated by a straight line for a short time. However, this approximation becomes unreliable fairly quickly.
Circular. Shoot at where the target will be, assuming that it moves at a constant velocity and a constant angular velocity. A little better than linear targeting. This targeting can be avoided by changing velocity quickly, such as stopping and starting and moving back and forth quickly.
Oscillatory. This strategy assumes that the target robot is moving back and forth continually. This targeting is very effective against robots with this kind of movement, as only a small number of robots use this kind of targeting.
Movement pattern matching. This approach records the past movements of the robot and assumes that the robot will move in a repeatable pattern. Shoot at the predicted future position. This kind of targeting has the greatest potential of any of the previous methods. Potential drawbacks are the processing time that it takes for an exhaustive search.
*/