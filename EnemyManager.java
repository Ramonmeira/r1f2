package rlm;

import robocode.*;
import java.util.*;

import rlm.GravPoint;
import rlm.Gun;

class EnemyManager{
	R1F2 bot;
	Hashtable targets;				//all enemies are stored in the hashtable
	GravPoint target;					//our current enemy
	int numBullets = 0;

	public EnemyManager (R1F2 _bot){
		bot = _bot;
		targets = new Hashtable();
		target = new GravPointEnemy();
		target.distance = 100000;//initialise the distance so that we can select a target
	}

	public GravPoint getTarget(){
		return target;
	}

	public Hashtable getTargets(){
		return targets;
	}

	public void updateEnemy(ScannedRobotEvent e){
		System.out.println(e.getName() + " " + bot.getTime());
		GravPoint enemy, bullet;
		double previousEnergy = 0;

		if (targets.containsKey(e.getName())) {
			enemy = (GravPoint)targets.get(e.getName());
			previousEnergy = enemy.energy;
		} else {
			enemy = new GravPointEnemy();
			targets.put(e.getName(),enemy);
			previousEnergy = e.getEnergy();
			System.out.println(enemy.timeOfScan + " " + bot.getTime());
		}

		//the next line gets the absolute bearing to the point where the bot is
		double absbearing_rad = (bot.getHeadingRadians()+e.getBearingRadians())%(2*Math.PI);
		//this section sets all the information about our target
		enemy.name = e.getName();
		double h = Util.normaliseBearing(e.getHeadingRadians() - enemy.heading);
		h = h/(bot.getTime() - enemy.timeOfScan);
		enemy.changehead = h;
		enemy.x = bot.getX()+Math.sin(absbearing_rad)*e.getDistance(); //works out the x coordinate of where the target is
		enemy.y = bot.getY()+Math.cos(absbearing_rad)*e.getDistance(); //works out the y coordinate of where the target is
		enemy.bearing = e.getBearingRadians();
		enemy.heading = e.getHeadingRadians();
		enemy.timeOfScan = bot.getTime();				//game time at which this scan was produced
		enemy.speed = e.getVelocity();
		enemy.distance = e.getDistance();
		enemy.energy = e.getEnergy();	
		if ((enemy.distance < target.distance)||(target.live == false)) {
			target = enemy;
		} 
		
		// Check if the target is firing
		// If the bot has small energy drop, assume it fired
		double changeInEnergy = previousEnergy - enemy.energy;
	    if (changeInEnergy > 0 && changeInEnergy <= 3) {
	    	bullet = new GravPointBullet();
	    	numBullets++;
	    	bullet.name = "BULLET" + numBullets;
			targets.put(bullet.name, bullet); 
			bullet.x = enemy.x;
			bullet.y = enemy.y;
			bullet.timeOfScan = bot.getTime();		
			bullet.energy = changeInEnergy;
			bullet.speed = Gun.getBulletVelocity(changeInEnergy);
			bullet.bearing = Util.normaliseBearing(Math.PI + enemy.bearing);
			bullet.print();
	    }
	    
	}

	public void onRobotDeath(RobotDeathEvent e) {
		GravPoint enemy = (GravPoint)targets.get(e.getName());
		enemy.live = false;
	}

	public void checkTargets(){
		GravPoint enemy;
		Enumeration e = targets.elements();
		while (e.hasMoreElements()) {
    	    enemy = (GravPoint)e.nextElement();
    	    enemy.print();
    	}
	}
}