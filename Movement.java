package rlm;

import java.util.*;

import rlm.GravPoint;

/**
 * All methods related to the Movement
 * <p>
 */
public class Movement {
	private R1F2 bot;
	double xforce = 0;
    double yforce = 0;
    Hashtable targets;
	Hashtable bullets = new Hashtable();	
	GravPoint[] pattern = new GravPoint[4];	

	public Movement(R1F2 _bot){
		bot = _bot;
		double w = bot.getBattleFieldWidth()/2;
		double h = bot.getBattleFieldHeight()/2;
		pattern[0] = new GravPointMidPoint(0*w,1*h);
		pattern[1] = new GravPointMidPoint(1*w,0*h);
		pattern[2] = new GravPointMidPoint(1*w,2*h);
		pattern[3] = new GravPointMidPoint(2*w,1*h);
	}
	
	void doMove(){
		//antiGravMove();
		bot.setTurnRight(10000);	
		bot.setAhead(10000);
	}

	void antiGravMove() {
   		xforce = 0;
	    yforce = 0;
		GravPoint enemy;
		Enumeration e;

		targets = bot.enemyManager.getTargets();	
    	e = targets.elements();
		while (e.hasMoreElements()) {
    	    enemy = (GravPoint)e.nextElement();
			if (enemy.live) {
				enemy.update(bot);
				removeOldBullet(enemy);
				updateForce(enemy);
			}
	    } 
	    for (int i = 0; i < 4; i++) {
			pattern[i].update(bot);
			updateForce(pattern[i]);
	    }
	   
	    /**The following four lines add wall avoidance.  They will only affect us if the bot is close 
	    to the walls due to the force from the walls decreasing at a power 3.**/
	    xforce += 5000/Math.pow(Util.getRange(bot.getX(), bot.getY(), bot.getBattleFieldWidth(), bot.getY()), 3);
	    xforce -= 5000/Math.pow(Util.getRange(bot.getX(), bot.getY(), 0, bot.getY()), 3);
	    yforce += 5000/Math.pow(Util.getRange(bot.getX(), bot.getY(), bot.getX(), bot.getBattleFieldHeight()), 3);
	    yforce -= 5000/Math.pow(Util.getRange(bot.getX(), bot.getY(), bot.getX(), 0), 3);
	    
	    //Point to move.
	    goTo(bot.getX()-xforce,bot.getY()-yforce);
	}

	public void goTo(double x, double y) {
		int direction;    	
	    double dist = 20; 
	    double angle = Util.absbearing(bot.getX(),bot.getY(),x,y);
	    double minimumAngle = Util.normaliseBearing(bot.getHeadingRadians() - angle);
	    
	    if (minimumAngle > Math.PI/2) {
	        minimumAngle -= Math.PI;
	        direction = -1;
	    }
	    else if (minimumAngle < -Math.PI/2) {
	        minimumAngle += Math.PI;
	        direction = -1;
	    }
	    else {
	        direction = 1;
	    }

	    double distance = dist * direction;

	    bot.setTurnLeft(minimumAngle);
		bot.setAhead(distance);
	}

	public void updateForce(GravPoint p){
	    double force;
	    double ang;
        force = p.power/Math.pow(Util.getRange(bot.getX(),bot.getY(),p.x,p.y),p.pow);
        //Find the bearing from the point to us
        ang = Util.normaliseBearing(Math.PI/2 - Math.atan2(bot.getY() - p.y, bot.getX() - p.x)); 
        //Add the components of this force to the total force in their respective directions
        xforce += Math.sin(ang) * force;
        yforce += Math.cos(ang) * force;
    }

    //remove old bullets
	public void removeOldBullet(GravPoint p){
		if(p.x < 0 ||
			p.y < 0 ||
			p.x > bot.getBattleFieldWidth() || 
			p.y > bot.getBattleFieldHeight()){
			targets.remove(p.name);
		}
	}   

}



/*Movement strategies
The following are levels of movement strategies:

Stay still. Easy to hit. Generally a bad idea.
Move in a straight line. Avoids being hit by Stationary targeting.
Move in a circular curve. Avoids being hit by Stationary and Linear targeting.
Move backwards and forwards in an oscillating motion. Difficult to hit using Linear and Circular targeting, but Stationary targeting can work quite well here.
Move in a random direction. Can be effective in reducing hits by all levels of targeting, but usually difficult to do in such a way as to not be hit.
Advanced movement. Use all kinds of data about the other robots to choose the best place to move to. This is by far the biggest and least understood area of robot strategy. It is one of the main differentiating factors between the best robots and the not-so-good robots.
*/

/*Additional enhancements
Anti-gravity is an incredibly flexible technique, so a discussion of the full range of behaviours you can produce with it is impractical. Here are, however, a few of the more interesting ones:

Target selection: By assigning lower repulsion values to targets that you are good at hitting or have low health, you can move closer to them and prey on the weak.

Randomisation: On a fairly regularly basis, you may want to add or subtract random amounts from your x and y forces to produce some more random movement, and even to stop occasionally, to fool enemy targeting systems. I encourage you to implement this behaviour.

Melee bullet dodging: If you know when an enemy is firing at you, you can model the bullet that's fired as an anti gravity point. If you assume that it was fired with, for example, linear targeting, you can update the position of that gravity point each turn and dodge the bullet. This enhancement, however, has yet to be perfected by any bot.

Follow the leader: This enhancement involves creating an attractive point for your robot to follow. You can thus produce any pattern that you want (within the the laws of robocode physics) by moving the point, all while leaving in the standard anti-gravity wall repulsion.*/

