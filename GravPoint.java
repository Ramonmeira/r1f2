package rlm;

import java.awt.geom.*;

/**
 * MyClass - a class by (your name here)
 */

abstract class GravPoint {
	//TODO Getters and Setters
	String name;
	public double bearing,heading,speed,distance,changehead,energy;
	public double x, y, power, pow;
	public long timeOfScan; 		
	public boolean live;

	public Point2D.Double guessPosition(R1F2 bot, long timeImpact) {
		double diff = timeImpact - timeOfScan;
		double newX = -1, newY = -1 ;
		while(newY < 0 || 
				newX < 0 ||  
				newY > bot.getBattleFieldWidth() || 
				newX > bot.getBattleFieldHeight()){
			newY = y + Math.cos(heading) * speed * diff;
			newX = x + Math.sin(heading) * speed * diff;
			diff--;
			if (diff == 0) break;
		}
		
		return new Point2D.Double(newX, newY);
	}

	public boolean isUpdated(long timeNow){
		return ((timeNow - timeOfScan) < 16);
	}

	public abstract void update(R1F2 bot);

	public void print(){
		System.out.println("name: " + name);
		System.out.println("bearing: " + bearing); 
		System.out.println("heading: " + heading);
		System.out.println("speed: " + speed);
		System.out.println("distance: " + distance + " changehead: " + changehead + " energy: " + energy);
		System.out.println("x:" + x + " y: " + y + " power: " + power + " pow: " + pow);
		System.out.println("timeOfScan: " + timeOfScan); 		
		System.out.println("live: " + live);
	}
}

class GravPointEnemy extends GravPoint {
	public GravPointEnemy(){
        x = 0;
        y = 0;
        live = true;
        power = 1000;
        pow = 2;
	}

    public void update(R1F2 bot){
    	
    }
}

class GravPointBullet extends GravPoint {
	public GravPointBullet(){
        x = 0;
        y = 0;
        power = -5000;
        pow = 2;
        live = true;
	}

    public void update(R1F2 bot){
    	double elapsedTime = (bot.getTime() - timeOfScan);
    	x += Math.sin(bearing) * speed * elapsedTime;
	    y += Math.cos(bearing) * speed * elapsedTime;
    	timeOfScan = bot.getTime();	
    	this.print();
    }
}

class GravPointMidPoint extends GravPoint {
	int midpointcount = 0;
	int SHIFTS = 5;

	public GravPointMidPoint(double x, double y){
        this.x = x;
        this.y = y;
        power = 0;
        pow = 1.5;
        live = true;
	}

    public void update(R1F2 bot){
		if (midpointcount > SHIFTS) {
			midpointcount = 0;
			power = (Math.random() * 2000) - 1000;
		}
    	midpointcount++;	
    }
}

class GravPointPattern extends GravPoint {
	public GravPointPattern(double x, double y){
        this.x = x;
        this.y = y;
        power = 4000;
        pow = 1.5;
        live = true;
	}

    public void update(R1F2 bot){
    	double p = 4000;
		if (x == 0) {
			if (bot.getX() < bot.getBattleFieldWidth()/2) {
				power = -p;
			}
			else{
				power = p;
			}
		}
		else if (x == bot.getBattleFieldWidth()/2) {
			if (y == 0) {
				if (bot.getY() < bot.getBattleFieldHeight()/2) {
					power = -p;
				}
				else{
					power = p;
				}
			}
			else{
				if (bot.getY() < bot.getBattleFieldHeight()/2) {
					power = p;
				}
				else{
					power = -p;
				}
			}
		}
		else{
			if (bot.getX() < bot.getBattleFieldWidth()/2) {
				power = p;
			}
			else{
				power = -p;
			}
		}	
    }
}
